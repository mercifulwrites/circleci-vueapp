import { describe, it, expect } from 'vitest'
import { mount } from '@vue/test-utils'
import HelloWorld from '../HelloWorld.vue'

describe('HelloWorld', () => {
  it('renders properly', () => {
    const wrapper = mount(HelloWorld, { props: { msg: 'Hello Vitest' } })
    expect(wrapper.text()).toContain('Hello Vitest')
  })

  it('contains inner text', () => {
    const wrapper = mount(HelloWorld, { props: { msg: 'Hello Vitest' }})
    expect(wrapper.text()).toContain("You’ve successfully created a project with")
  })


  it('contains msg prop as string', () => {
    const wrapper = mount(HelloWorld, {
        props:{
            msg: 'Hello Vitest',
        },
    })
    expect(wrapper.text()).toBeTypeOf('string')
  })

})